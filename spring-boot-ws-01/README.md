spring-boot-ws-sample
=====================

Spring Boot and Spring-WS integration 

### Description

- one application context shared between Spring MVC and Spring WS -  ```WebServiceConfig``` class
- Spring-WS configured with ```sws``` namespace
- class generation with ```jaxb2-maven-plugin``` 


### Launch

```mvn spring-boot:run```