package demo.spring.ws;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class ApplicationTests {
	@Autowired
	@Qualifier("ws")
	private ServletRegistrationBean messageDispatcherServletRegistrationBean;

	@Test
	public void should_register_message_dispatcher_servlet() {
		assertNotNull(messageDispatcherServletRegistrationBean);
	}
}
